
MACRO(find_avrdude_prog_part _mcu)
   if(${_mcu} STREQUAL "atmega32")
      set(AVRDUDE_PROG_PART "m32")
   elseif(${_mcu} STREQUAL "atmega16")
      set(AVRDUDE_PROG_PART "m16")
   else()
      set(AVRDUDE_PROG_PART-NOTFOUND TRUE)
   endif()
ENDMACRO(find_avrdude_prog_part)

find_avrdude_prog_part(${CMAKE_SYSTEM_PROCESSOR})

IF(AVRDUDE_PROG_PART-NOTFOUND)
   message(FATAL_ERROR  "Programmer target type not supported!")
ENDIF(AVRDUDE_PROG_PART-NOTFOUND)

IF(NOT PROGRAMMER_ID OR NOT PROGRAMMER_PORT)
   message(WARNING "Programmer Id (PROGRAMMER_ID) and Port (PROGRAMMER_PORT) is unspecified. Default value (usbasp) will be used!" )
   SET(PROGRAMMER_ID "usbasp")
   SET(PROGRAMMER_PORT "usbasp")
ENDIF(NOT PROGRAMMER_ID OR NOT PROGRAMMER_PORT)

find_program(AVRDUDE_CMD avrdude)
IF(${AVRDUDE_CMD-NOTFOUND})
   message(WARNING "'avrdude' program not found. 'upload_*', dump_* and 'info' targets will not be available!")
ELSE(${AVRDUDE_CMD-NOTFOUND})

SET(HEXFORMAT "ihex")
add_custom_target(upload_flash
   ${AVRDUDE_CMD}
      -c ${PROGRAMMER_ID}
      -p ${AVRDUDE_PROG_PART}
      -P ${PROGRAMMER_PORT} -e
      -U flash:w:${PROJECT_NAME}.hex
   DEPENDS ${PROJECT_NAME}.hex
   VERBATIM)

add_custom_target(upload_eeprom
   ${AVRDUDE_CMD}
      -c ${PROGRAMMER_ID}
      -p ${AVRDUDE_PROG_PART}
      -P ${PROGRAMMER_PORT} -e
      -U eeprom:w:${PROJECT_NAME}.ee.hex
   DEPENDS ${PROJECT_NAME}.ee.hex
   VERBATIM)

add_custom_target(update_fuse
   ${AVRDUDE_CMD}
      -c ${PROGRAMMER_ID}
      -p ${AVRDUDE_PROG_PART}
      -P ${PROGRAMMER_PORT} -e
      -U lfuse:w:0xe1:m -U hfuse:w:0x99:m 
   VERBATIM)

add_custom_target(dump_fuse
   ${AVRDUDE_CMD} -v
      -c ${PROGRAMMER_ID}
      -p ${AVRDUDE_PROG_PART}
      -P ${PROGRAMMER_PORT} -e
      -U hfuse:r:high.txt:r -U lfuse:r:low.txt:r
   VERBATIM)

add_custom_target(dump_eeprom
   ${AVRDUDE_CMD} -v
      -c ${PROGRAMMER_ID}
      -p ${AVRDUDE_PROG_PART}
      -P ${PROGRAMMER_PORT} -e
      -U eeprom:r:eepromdump.hex:i
   VERBATIM)

add_custom_target(dump_flash
   ${AVRDUDE_CMD} -v
      -c ${PROGRAMMER_ID}
      -p ${AVRDUDE_PROG_PART}
      -P ${PROGRAMMER_PORT} -e
      -U flash:r:flashdump.hex:i
   VERBATIM)

add_custom_command(
   OUTPUT ${PROJECT_NAME}.hex
   COMMAND /usr/bin/${CMAKE_SYSTEM_NAME}-objcopy --strip-all -j .text -j .data -O ${HEXFORMAT} ${PROJECT_NAME} ${PROJECT_NAME}.hex
   DEPENDS ${PROJECT_NAME}
   VERBATIM
)


add_custom_command(
   OUTPUT ${PROJECT_NAME}.ee.hex
   COMMAND /usr/bin/${CMAKE_SYSTEM_NAME}-objcopy --strip-all -j .eeprom --change-section-lma .eeprom=0 -O ${HEXFORMAT} ${PROJECT_NAME} ${PROJECT_NAME}.ee.hex
   DEPENDS ${PROJECT_NAME}
   VERBATIM
)

ENDIF(${AVRDUDE_CMD-NOTFOUND})

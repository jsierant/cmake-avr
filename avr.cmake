cmake_minimum_required(VERSION 2.8)

# mandatory:
# AVR_PROCESSOR  - gcc avr mcu name
# AVR_ROOT - default ("/usr/lib/avr")
# optional:
# AVR_C_COMPILER - C compiler
# AVR_CXX_COMPILER - C++ compiler

SET(CMAKE_SYSTEM_NAME "avr") #CMAKE_CROSSCOMPILING will be set to true

IF(NOT AVR_PROCESSOR)
   message(FATAL_ERROR  "GCC AVR MCU name is unspecyfied. Please set AVR_PROCESSOR variable!")
ENDIF(NOT AVR_PROCESSOR)
SET(CMAKE_SYSTEM_PROCESSOR ${AVR_PROCESSOR})

IF(NOT AVR_C_COMPILER)
   SET(AVR_C_COMPILER "avr-gcc")
ENDIF(NOT AVR_C_COMPILER)
IF(NOT AVR_CXX_COMPILER)
   SET(AVR_CXX_COMPILER "avr-g++")
ENDIF(NOT AVR_CXX_COMPILER)

SET(CMAKE_C_COMPILER ${AVR_C_COMPILER})
SET(CMAKE_CXX_COMPILER ${AVR_CXX_COMPILER})


# where is the target environment
IF(NOT CMAKE_FIND_ROOT_PATH)
   SET(${AVR_ROOT} "/usr/lib/avr")
ENDIF(NOT CMAKE_FIND_ROOT_PATH)

SET(CMAKE_FIND_ROOT_PATH  ${AVR_ROOT})

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)



# compiler additional options
SET(CXX_STD_OPT "-std=c++11")
SET(C_STD_OPT "-std=gnu99")

SET(CWARN "-Wall -Wstrict-prototypes -pedantic")
SET(CXXWARN "-Wall -Wno-attributes -pedantic")

SET(C_TUNING_OPT "-funsigned-char -funsigned-bitfields -fpack-struct -fshort-enums")
SET(CXX_TUNING_OPT "-fno-exceptions -fno-rtti -funsigned-char -funsigned-bitfields -fshort-enums")

# Update release build type
# SET(CMAKE_CXX_FLAGS_RELEASE "-ffunction-sections -fdata-sections -Os -DNDEBUG")
# SET(CMAKE_C_FLAGS_RELEASE ${CMAKE_CXX_FLAGS_RELEASE})
# SET(CMAKE_SHARED_LINKER_FLAGS_RELEASE "")
# SET(CMAKE_EXE_LINKER_FLAGS_RELEASE "-Os")
SET(CMAKE_SHARED_LIBRARY_LINK_CXX_FLAGS "")

SET(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} -mmcu=${CMAKE_SYSTEM_PROCESSOR} ${CWARN} ${C_STD_OPT} ${C_TUNING_OPT} -DCPU_FREQ=${AVR_CPU_FREQ} -DF_CPU=${AVR_CPU_FREQ}")
SET(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mmcu=${CMAKE_SYSTEM_PROCESSOR} ${CXXWARN} ${CXX_STD_OPT} ${CXX_TUNING_OPT} -DCPU_FREQ=${AVR_CPU_FREQ} -DF_CPU=${AVR_CPU_FREQ}")
SET(CMAKE_EXE_LINKER_FLAGS "-Wl,-Map,${PROJECT_NAME}.map -lm -Wl,--gc-sections")

SET(AVR_INCLUDE_DIR ${CMAKE_FIND_ROOT_PATH}/include)

include_directories(${AVR_INCLUDE_DIR} ${AVR_INCLUDE_DIR}/avr ${AVR_INCLUDE_DIR}/compat ${AVR_INCLUDE_DIR}/util)


SET(GDBINITFILE "gdb-init")

add_custom_target(gdbinit
	echo file ${PROJECT_SOURCE_DIR}/build/${PROJECT_NAME} > ${GDBINITFILE}
	COMMAND echo target remote localhost:1212 >> ${GDBINITFILE}
	COMMAND echo load >> ${GDBINITFILE}
	COMMAND echo break main >> ${GDBINITFILE}
	COMMAND echo continue >> ${GDBINITFILE}
	VERBATIM)
